package spb.nio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WorkWithFiles
{
    /**
     * В данном методе необходимо пройти по каталогу и вывести в консоль следующие значение:
     *      - количество файлов в каталоге (включая во вложенных каталогах)
     * @param path - путь к каталогу, по которому необходимо предоставить информацию
     */
    public void walkingDirectory(Path path)
    {
        Path newPath = path;
        try{

            System.out.println(Files.walk(newPath)
                    .filter(p->Files.isRegularFile(p))
                    .count());

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * В данном методе необходимо все логи уровня WARN из исходного файла скопировать в новый файл.
     * Пример содержания исходного файла лога:
     *  INFO Server starting
     *  DEBUG Processes available = 10
     *  WARN No database could be detected
     *  DEBUG Processes available reset to 0
     *  WARN Performing manual recovery
     *  INFO Server successfully started
     * @param sourceFile - исходный файл с логом
     * @param destinationFile - целевой файл
     */
    public void warningsPrinting(Path sourceFile, Path destinationFile){

        try(BufferedReader bufferReader = Files.newBufferedReader(sourceFile, Charset.forName("UTF8"));
            BufferedWriter bufferWriter = Files.newBufferedWriter(destinationFile, Charset.forName("UTF8"))){
            String currentLine = null;
            while((currentLine=bufferReader.readLine()) != null) {
                if(currentLine.startsWith("WARN")){
                   bufferWriter.write(currentLine + "\n");
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * В данном методе необходимо реализовать создание копии файла средствами пакета {@link java.nio}
     * @param path - исходный файл
     * @return - true при успешном создании копии
     */
    public boolean copyFile(Path path)
    {
        try{
            Files.copy(path, Paths.get(path.toString()
                    .substring(0, path.toString()
                            .length()-4) + "-copy.txt"));
        }catch(IOException e){
            e.printStackTrace();
        }
        return true;
    }
}
