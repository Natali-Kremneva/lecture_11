package spb.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WorksWithPaths
{
    /**
     * Создать объект класса {@link Path}, проверить существование и чем является (файл или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать {@link Path}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithPath(String fileName)
    {
        Path path = Paths.get(fileName);

        if(Files.exists(path)){
            System.out.println(path.toAbsolutePath());
            System.out.println(path.getParent());
            if(Files.isRegularFile(path)){
                try {
                    System.out.println(Files.size(path));
                    System.out.println(Files.getLastModifiedTime(path));
                }catch (IOException ioe){
                    ioe.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }
}
