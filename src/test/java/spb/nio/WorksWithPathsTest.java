package spb.nio;

import org.junit.Assert;
import org.junit.Test;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;



public class WorksWithPathsTest {

    /**
     * Проверятся существование файла при возвращаемом значении false
     */
    @Test
    public void workWithPathWhenFalse_Test(){

        String str1 = "C:\\code\\Новая папка\\lecture11\\TextDoesNotExist.txt";
        Path path1 = Paths.get(str1);

        WorksWithPaths wwp = new WorksWithPaths();

        Assert.assertFalse(Files.exists(path1));
        Assert.assertTrue(!Files.exists(path1));
        Assert.assertEquals(false, wwp.workWithPath(str1));

    }


    /**
     * Проверятся существование файла при возвращаемом значении true
     */
    @Test
    public void workWithPathWhenTrue_Test(){

        WorksWithPaths wwp = new WorksWithPaths();

        String str2 = "C:\\code\\Новая папка\\lecture11\\Text.txt";
        String newStr = "If you want to be somebody,\n" +
                "somebody really special,\n" +
                "be yourself.\n" + "--------------------------------------\n" +
                "There are only two ways to live your life.\n" +
                "One is as though nothing is a miracle.\n" +
                "The other is as though everything is a miracle.\n";

        Path path2 = Paths.get(str2);
        try{
            byte[] bytes = newStr.getBytes();
            Files.write(path2, bytes);
        } catch (IOException e){
            e.printStackTrace();
        }

        wwp.workWithPath(str2);

        Assert.assertTrue(Files.exists(path2));
        Assert.assertEquals(true, Files.isRegularFile(path2));
    }

}