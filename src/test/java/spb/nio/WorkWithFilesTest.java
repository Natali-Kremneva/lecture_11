package spb.nio;

import org.junit.Assert;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


public class WorkWithFilesTest {


    /**
     * Проверяется соответствие вывода и ожидаемого значения количества файлов
     */
    @Test
    public void walkingDirectory_Test() {
        Path path = Paths.get("C:\\code\\Новая папка\\lecture11\\src");

        WorkWithFiles wwp = new WorkWithFiles();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        wwp.walkingDirectory(path);

        Assert.assertEquals("4", out.toString().trim());
        Assert.assertNotEquals("5", out.toString().trim());
    }


    /**
     * Проверяется создание нового файла
     * Проверяется является ли созданная сущность файлом
     * Проверяется соответствие записанных данных, согласно заданию
     */
    @Test
    public void warningsPrinting() {

        WorkWithFiles wwp = new WorkWithFiles();
        Path sourceFile = Paths.get("C:\\code\\Новая папка\\lecture11\\WorkWithFilesTest.log");
        Path destinationFile = Paths.get("C:\\code\\Новая папка\\lecture11\\WorkWithFilesTest-copy.log");

        wwp.warningsPrinting(sourceFile, destinationFile);

        Assert.assertEquals(true, Files.exists(destinationFile));
        Assert.assertEquals(true, Files.isRegularFile(destinationFile));
        List<String> list = null;

        try {
            list = Files.lines(destinationFile).collect(Collectors.toList());
        } catch (IOException e){
            e.printStackTrace();
        }
        for (String str:list) {
            Assert.assertEquals(true, str.startsWith("WARN"));
            Assert.assertNotEquals(true, str.startsWith("INFO"));
            Assert.assertNotEquals(true, str.startsWith("DEBUG"));
        }
    }


    /**
     * Проверяется создание нового файла
     * Проверяется является ли созданная сущность файлом
     * Проверяется соответствие записанных в новом файле данных с данными старого файла
     */
    @Test
    public void copyFile() {
        String str = "C:\\code\\Новая папка\\lecture11\\Text.txt";
        Path path = Paths.get(str);

        WorkWithFiles wwp = new WorkWithFiles();

        wwp.copyFile(path);

        String str2 = str.substring(0, str.length()-4) + "-copy.txt";
        Path newPath = Paths.get(str2);

        Assert.assertEquals(true, Files.exists(newPath));
        Assert.assertEquals(true, Files.isRegularFile(newPath));

        try {
            List<String> pathList = Files.lines(path).collect(Collectors.toList());
            List<String> newPathList = Files.lines(newPath).collect(Collectors.toList());
            for (int i = 0; i < pathList.size(); i++){
                Assert.assertEquals(true, pathList.get(i).equals(newPathList.get(i)));
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}